const moment = require('moment');

class MessagesHandler {
  constructor() {};

  handleBackedUpMessages(client) {
    const startTime = moment(new Date()).format("HH:mm:ss");
    client.keys('*', (error, keys) => {
      if (error) {
        console.log(error);
      } else {
        keys.forEach((key) => {
          if (moment(key, "HH:mm:ss").isBefore(moment(startTime, "HH:mm:ss"))) {
            this.logMessages(client, key);
          }
        });
      }
    });
  }

  logMessages(client, key) {
    client.lrange(key, 0, -1, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        data.forEach((item) => {
          console.log(item);                
        });
        this.deleteMessage(client, key);
      }
    });
  }

  deleteMessage(client, key) {
    client.del(key, (error) => {
      if (error) {
        console.log(error);
      }
    });
  }

  handleNewMessages(client) {
    const startTime = moment(new Date()).format("HH:mm:ss");
    client.exists(startTime, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        this.logMessages(client, startTime);
      }
    });
  }
}

module.exports = new MessagesHandler();
