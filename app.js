const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const redis = require('redis');

const messagesHandler = require('./messagesHandler');
const echoAtTime = require('./routes/echoAtTime');

//Redis Config
const client = redis.createClient();
client.on('connect', () => {
  console.info('Redis connected.');
});

//Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//Routes config
app.use('/', echoAtTime);

//Server start
app.listen(3000, () => {
  console.info('Server started on: http://localhost:3000');
  messagesHandler.handleBackedUpMessages(client);
  setInterval(() => { messagesHandler.handleNewMessages(client) }, 1000);
});
