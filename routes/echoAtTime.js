const express = require('express');
const redis = require('redis');

//Redis Config
const client = redis.createClient();

//Router Config
const router = express.Router();

//Save message route
router.post('/', (req, res, next) => {
  const time = req.body.time;
  const message = req.body.message;
  client.rpush([time, message], (error, data) => {
    if (error) {
      console.log(error);
    } else {
      res.send('Message has been saved.');
    }
  });
});

module.exports = router;
